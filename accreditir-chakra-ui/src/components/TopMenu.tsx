import { Flex, useColorMode, FlexProps } from '@chakra-ui/react';

export const TopMenu = (props: FlexProps) => {
  const { colorMode } = useColorMode()

  const bgColor = { light: '#242424', dark: '#242424' }

  const color = { light: 'black', dark: 'white' }
  return (
    <Flex
      direction="row"
      alignItems="stretch"
      justifyContent="flex-start"
      bg={bgColor[colorMode]}
      color={color[colorMode]}
      border="1px"
      borderColor="#242424 #242424 #01ACF4 #242424"
      {...props}
    />
  )
}
