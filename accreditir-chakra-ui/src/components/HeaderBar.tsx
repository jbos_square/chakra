import {
  Flex,
  useColorMode,
  FlexProps,
  Center,
  IconButton,
  Heading,
} from '@chakra-ui/react';
import {
  HamburgerIcon,
} from '@chakra-ui/icons';

export const HeaderBar = (props: FlexProps) => {
  const { colorMode } = useColorMode()

  const bgColor = { light: '#242424', dark: '#242424' }

  const color = { light: 'black', dark: 'white' }
  return (
    <Flex
      direction="row"
      alignItems="left"
      justifyContent="flex-start"
      bg={bgColor[colorMode]}
      color={color[colorMode]}
      border="1px"
      borderColor="#242424 #242424 #01ACF4 #242424"
      {...props}
    >
      <Center p="4">
        <IconButton
          size="lg"
          aria-label="Menu Options"
          icon={<HamburgerIcon fontSize="48px" color="white" />}
        />
      </Center>
      <Center p="1">
        <img width="130" height="200" src="Accredit.png" alt="Accredit Icon" />
      </Center>
      <Center p="10">
        <Heading size="md">
          Integrated Reporting
        </Heading>
      </Center>
    </Flex>
  )
}
