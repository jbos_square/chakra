import { Flex, useColorMode, FlexProps } from '@chakra-ui/react';

export const ContainerPage = (props: FlexProps) => {
  const { colorMode } = useColorMode()

  const bgColor = { light: 'green.50', dark: 'green.900' }

  const color = { light: 'black', dark: 'white' }
  return (
    <Flex
      direction="row"
      alignItems="center"
      justifyContent="flex-start"
      basis="0"
      bg={bgColor[colorMode]}
      color={color[colorMode]}
      {...props}
    />
  )
}
