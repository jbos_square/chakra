import React, { useState } from 'react';
import {
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionPanel,
    Flex,
    Center,
    Square,
    Icon,
    EditablePreview,
    EditableInput,
    Editable,
    Spacer,
    IconButton,
} from '@chakra-ui/react';
import {
    AddIcon,
    MinusIcon,
} from '@chakra-ui/icons';
import {
    BsFillCaretRightFill,
    BsFillCaretDownFill,
} from "react-icons/bs";

import { SubTopic } from './SubTopic';

export const Topic = ({ title, id, remove }: { title: string, id: number, remove: (e: any, pos: number) => void }) => {
    const startId = 1;
    const startTitle = "Subtopic - 1";
    const [subTopics, setSubTopics] = useState([{ id: startId, title: startTitle }]);

    function addSubTopic(e: any) {
        e.preventDefault();

        const lastSubTopicId = subTopics.length > 0 ? (subTopics[subTopics.length - 1].id + 1) : 1;
        const titleSubTopic = "Subtopic - " + lastSubTopicId;
        const newSubTopic = { id: lastSubTopicId, title: titleSubTopic };

        setSubTopics(oldArray => [...oldArray, newSubTopic]);
    }

    function removeSubTopic(e: any, pos: number) {
        e.preventDefault();
        const filtered = subTopics.filter(item => item.id !== pos);
        setSubTopics(filtered);
    };

    return (
        <Accordion borderColor="#242424 #242424 #242424 #242424" allowToggle allowMultiple>
            <AccordionItem>
                {({ isExpanded }) => (
                    <>
                        <h2>
                            <Flex>
                                <Center size="25px">
                                    <AccordionButton size="25px">
                                        {isExpanded ? (
                                            <Icon as={BsFillCaretDownFill} w={3} h={3} color="#01ACF4" />
                                        ) : (
                                            <Icon as={BsFillCaretRightFill} w={3} h={3} color="#01ACF4" />
                                        )}
                                    </AccordionButton>
                                </Center>
                                <Square >
                                    <Editable defaultValue={title} size="100px">
                                        <EditablePreview />
                                        <EditableInput />
                                    </Editable>
                                </Square>
                            </Flex>
                            <Flex>
                                <IconButton
                                    size="xs"
                                    onClick={(e) => addSubTopic(e)}
                                    aria-label="Add Subtopic"
                                    icon={<AddIcon fontSize="12px" color="#01ACF4" />}
                                />
                                <Spacer />
                                <IconButton
                                    size="xs"
                                    onClick={(e) => { remove(e, id) }}
                                    aria-label="Delete Topic"
                                    icon={<MinusIcon fontSize="12px" color="#01ACF4" />}
                                />
                            </Flex>
                        </h2>
                        <AccordionPanel pb={4}>
                            <ul>
                                {
                                    subTopics.map(subtopic => (
                                        <SubTopic key={subtopic.id} id={subtopic.id} title={subtopic.title} remove={removeSubTopic} />
                                    ))
                                }
                            </ul>
                        </AccordionPanel>
                    </>
                )}
            </AccordionItem>
        </Accordion>
    );
};