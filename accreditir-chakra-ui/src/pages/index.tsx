import { Container } from '../components/Container'

import { ToolsMenu } from '../components/ToolsMenu';
import { ContainerPage } from '../components/Container-page';
import { HeaderBar } from '../components/HeaderBar';
import { TopicMenu } from '../components/TopicMenu';

const Index = () => (
  <Container minHeight="100vh" height="100vh" width="100%" >
    <HeaderBar minHeight="8vh" height="8%" width="100%" />
    <ContainerPage minHeight="92vh" height="92%" width="100%">
      <TopicMenu height="100%" width="12%" />
      <Container height="100%" width="78%" />
      <ToolsMenu height="100%" width="10%" />
    </ContainerPage>
  </Container>
);

export default Index;
