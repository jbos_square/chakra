import {
  Flex, useColorMode, FlexProps,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  Center,
  Square,
  Icon,
  EditablePreview,
  EditableInput,
  Editable,
  IconButton,
} from '@chakra-ui/react';

import {
  AddIcon
} from '@chakra-ui/icons';

import React, { useState } from 'react';

import {
  BsFillCaretRightFill,
  BsFillCaretDownFill,
} from "react-icons/bs";

import { Topic } from './Topic';

export const TopicMenu = (props: FlexProps) => {
  const { colorMode } = useColorMode()

  const bgColor = { light: '#242424', dark: '#242424' }
  //const bgColor = { light: 'gray.300', dark: 'gray.800' }

  const color = { light: 'black', dark: 'white' }

  const startId = 1;
  const startTitle = "Topic - 1";
  const [topics, setTopics] = useState([{ id: startId, title: startTitle }]);

  function addTopic(e: any) {
    e.preventDefault();

    const lastTopicId = topics.length > 0 ? (topics[topics.length - 1].id + 1) : 1;
    const titleTopic = "Topic - " + lastTopicId;
    const newTopic = { id: lastTopicId, title: titleTopic };

    setTopics(oldArray => [...oldArray, newTopic]);
  }

  function removeTopic(e: any, pos: number) {
    e.preventDefault();
    const filtered = topics.filter(item => item.id !== pos);
    setTopics(filtered);
  };

  return (
    <Flex
      direction="column"
      alignItems="center"
      //justifyContent="flex-start"
      bg={bgColor[colorMode]}
      color={color[colorMode]}
      border="1px"
      borderColor="#242424 #01ACF4 #242424 #242424"
      overflowY="auto"

      css={{
        '&::-webkit-scrollbar': {
          width: '4px',
        },
        '&::-webkit-scrollbar-track': {
          boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
          webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
        },
        '&::-webkit-scrollbar-thumb': {
          background: '#90cdf4',
          borderRadius: '24px',
        },
      }}

      {...props}
    >
      <Accordion borderColor="#242424 #242424 #242424 #242424" allowToggle allowMultiple width="95%">
        <AccordionItem>
          {({ isExpanded }) => (
            <>
              <h2>
                <Flex>
                  <Center size="25px">
                    <AccordionButton size="25px">
                      {isExpanded ? (
                        <Icon as={BsFillCaretDownFill} w={3} h={3} color="#01ACF4" />
                      ) : (
                        <Icon as={BsFillCaretRightFill} w={3} h={3} color="#01ACF4" />
                      )}
                    </AccordionButton>
                  </Center>
                  <Square extAlign="left">
                    <Editable defaultValue="Report" size="100px">
                      <EditablePreview />
                      <EditableInput />
                    </Editable>
                  </Square>
                </Flex>
                <IconButton
                  size="xs"
                  onClick={(e) => addTopic(e)}
                  aria-label="Add Subtopic"
                  icon={<AddIcon fontSize="12px" color="#01ACF4" />}
                />
              </h2>
              <AccordionPanel pb={4}>
                <ul>
                  {
                    topics.map(topic => (
                      <Topic key={topic.id} id={topic.id} title={topic.title} remove={removeTopic} />
                    ))
                  }
                </ul>
              </AccordionPanel>
            </>
          )}
        </AccordionItem>
      </Accordion>
    </Flex>
  )
}