import React, { useState } from 'react';
import {
    Accordion,
    AccordionItem,
    AccordionButton,
    Box,
    Flex,
    Center,
    Icon,
    EditablePreview,
    EditableInput,
    Editable,
    IconButton,
    Spacer,
} from '@chakra-ui/react';
import {
    MinusIcon,
} from '@chakra-ui/icons';
import {
    BsFillCaretRightFill,
} from "react-icons/bs";

export const SubTopic = ({ title, id, remove }: { title: string, id: number, remove: (e: any, pos: number) => void }) => {
    return (
        <Accordion borderColor="#242424 #242424 #242424 #242424" allowToggle allowMultiple>
            <AccordionItem>
                <h2>
                    <Flex>
                        <Center size="25px">
                            <AccordionButton size="25px">
                                <Icon as={BsFillCaretRightFill} w={3} h={3} color="#01ACF4" />
                            </AccordionButton>
                        </Center>
                        <Box flex="1" textAlign="left">
                            <Editable defaultValue={title} size="sm">
                                <EditablePreview />
                                <EditableInput />
                            </Editable>
                        </Box>
                    </Flex>
                </h2>
                <Flex>
                    <span />
                    <Spacer />
                    <IconButton
                        size="xs"
                        onClick={(e) => { remove(e, id) }}
                        aria-label="Delete Subtopic"
                        icon={<MinusIcon fontSize="12px" color="#01ACF4" />}
                    />
                </Flex>
            </AccordionItem>
        </Accordion>
    );
};
