import { Flex, useColorMode, FlexProps } from '@chakra-ui/react';

export const ToolsMenu = (props: FlexProps) => {
  const { colorMode } = useColorMode()

  const bgColor = { light: '#242424', dark: '#242424' }

  const color = { light: 'black', dark: 'white' }
  return (
    <Flex
      direction="column"
      alignItems="center"
      justifyContent="flex-start"
      bg={bgColor[colorMode]}
      color={color[colorMode]}
      border="1px" borderColor="#242424 #242424 #01ACF4 #01ACF4"
      overflowY="auto"
      {...props}
    />
  )
}
